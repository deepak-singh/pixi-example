var height = 500
var width = 500

function random (divisbleBy, min, max) {
	return Math.round((Math.random()*(max-min)+min)/divisbleBy)*divisbleBy;
}


start = {x:250, y:40}
firstJunction = {x:250, y:100}
lane1 = {x:150, y:150}
lane2 = {x:350, y:150}
secondJunction = {x:300, y:350}

var stroke1 = [{x: 250, y: 40}, 
				{x: 250, y: 100}]
				

var stroke2 = [{x: (stroke1[1].x + 150) / 2, y: (stroke1[1].y + 150) / 2},
				{x:150, y:150},
				{x:150, y:250}]


var stroke3 = [{x: (stroke1[1].x + 350) / 2, y:(stroke1[1].y + 150) / 2},
				{x:350, y:150},
				{x:350, y:250}]

var bridgePath = [{},{}]

var path = [stroke1, {}, {}]


var renderer = PIXI.autoDetectRenderer(width, height,{ antialias: true })
document.getElementById('main').appendChild(renderer.view)

var stage = new PIXI.Container()
var graphics = new PIXI.Graphics()
var circle = new PIXI.Graphics()
var bridge = new PIXI.Graphics()

stage.addChild(circle)
stage.addChild(graphics)
stage.addChild(bridge)

graphics.lineStyle(5, 0x00f2ff)	
bridge.lineStyle(5, 0x00f2ff)	
circle.beginFill(0xfc9f9f)

circle.drawCircle(firstJunction.x, firstJunction.y, 10)
circle.endFill()
circle.hitArea = new PIXI.Circle(firstJunction.x, firstJunction.y, 10)
circle.interactive = true
circle.buttonMode = true


function skeletonPath () {
	for(var i in stroke1) {
		if( i == 0){
			graphics.moveTo(stroke1[i].x, stroke1[i].y)
		} else {	
			graphics.lineTo(stroke1[i].x, stroke1[i].y)
		}
	}

	for(var i in stroke2) {
		if( i == 0){
			graphics.moveTo(stroke2[i].x, stroke2[i].y)
		} else {	
			graphics.lineTo(stroke2[i].x, stroke2[i].y)
		}
	}

	for(var i in stroke3) {
		if( i == 0){
			graphics.moveTo(stroke3[i].x, stroke3[i].y)
		} else {	
			graphics.lineTo(stroke3[i].x, stroke3[i].y)
		}
	}
	graphics.endFill()
}

function connectStroke1n2() {
	bridgePath[0] = {x:stroke1[1].x, y: stroke1[1].y}
	bridgePath[1] = {x:stroke2[0].x, y:stroke2[0].y}
	path[1] = bridgePath
	path[2] = stroke2
	bridge.moveTo(stroke1[1].x, stroke1[1].y)
	bridge.lineTo(stroke2[0].x, stroke2[0].y)
	bridge.endFill()
}

function connectStroke1n3() {
	bridgePath[0] = {x:stroke1[1].x, y: stroke1[1].y}
	bridgePath[1] = {x:stroke3[0].x, y:stroke3[0].y}
	path[1] = bridgePath
	path[2] = stroke3
	bridge.moveTo(bridgePath[0].x, bridgePath[0].y)
	bridge.lineTo(bridgePath[1].x, bridgePath[1].y)
	bridge.endFill()
}


skeletonPath()
var yes = true
connectStroke1n2()

circle.click = function(mouseData){
	bridge.destroy()
	bridge = new PIXI.Graphics()
	stage.addChild(bridge)
	bridge.lineStyle(5, 0x00f2ff)	
	if(yes) {
		console.log('connect 1 and 3')
		connectStroke1n3()
		yes = false
    }
    else {
		console.log('connect 1 and 2')
    	connectStroke1n2()
		yes = true
    }
};

renderer.render(stage);


requestAnimationFrame(animate)
function animate() {
	renderer.render(stage)
 	requestAnimationFrame(animate)	
}