var height = 500
var width = 500

function random (divisbleBy, min, max) {
	return Math.round((Math.random()*(max-min)+min)/divisbleBy)*divisbleBy;
}

var radius = 10
var point_a_r = radius
var point_a_x = random(100, radius*2, width-radius)
var point_a_y = radius

var point_b_r = radius
var point_b_x = random(100, radius*2, width-radius)
var point_b_y = height - radius

console.log("ax: "+point_a_x)
console.log("bx: "+point_b_x)

var path = []

var renderer = PIXI.autoDetectRenderer(width, height)
document.getElementById('main').appendChild(renderer.view)

var stage = new PIXI.Container()
var graphics = new PIXI.Graphics()

var player = PIXI.Sprite.fromImage('image.png');
player.anchor.set(0.5)
player.scale.set(0.1)
player.blendMode = PIXI.BLEND_MODES.ADD;
player.position.x = point_a_x
player.position.y = point_a_y
player.speed = 1 
console.log(player)
stage.addChild(player)

// dude.anchor.set(0.5);


stage.addChild(graphics)
graphics.lineStyle(1, 0x00f2ff)	

pointA()
pointB()
generatePath()
graphics.moveTo(0, 0)
drawPath()
graphics.endFill();

renderer.render(stage);

function pointA () {
	graphics.drawCircle(point_a_x, point_a_y, radius)
}

function pointB () {
	graphics.drawCircle(point_b_x, point_b_y, radius)
}

function drawPath () {
	graphics.moveTo(path[0].x, path[0].y)
	for (var i = 1; i < path.length; i++) {
		graphics.lineTo(path[i].x, path[i].y)
		graphics.moveTo(path[i].x, path[i].y)
	}
}

function generatePath() {
	path.push({x: point_a_x, y: point_a_y+radius}) //starting point
	var forX = true
	for (var i = point_a_y+radius; i < point_b_y; i+=radius) {
		// console.log(i)
		if(forX) {
			path.push({x: random(radius, radius, width-radius), y: path[path.length - 1].y})
			forX = false
		} else {
			console.log('point_by:' + point_b_y)
			console.log(i)
			path.push({x: path[path.length - 1].x, y: i})	
			forX = true
		}
	}
	
	path.push({x: point_b_x, y: point_b_y-radius}) //end point
}




var tick = 1
requestAnimationFrame(animate)

console.log("initally"+player.position.x+" "+player.position.y)
console.log("initally"+path[tick].x+" "+path[tick].y)
function animate() {

	if( tick < path.length){
		if(player.position.x > path[tick].x){
			for(var i=player.position.x; i>path[tick].x; i--) {
				// console.log("player position x:"+player.position.x)
				// console.log("to reach on x :"+path[tick].x)
				player.position.x -= 0.5
				player.position.x = Math.round(player.position.x * 10000) / 10000
				console.log(player.position.x)
			}	
		}
		else {
			for(var i=player.position.x; i<path[tick].x; i++) {
				// console.log("player position x:"+player.position.x)
				// console.log("to reach on x :"+path[tick].x)
				player.position.x += 0.5
				player.position.x = Math.round(player.position.x * 10000) / 10000
				console.log(player.position.x)
			}	
		}
		// player.position.x = path[tick].x
		
		for(var j=player.position.y; j<path[tick].y; j++) {
			// console.log("player position y:"+player.position.y)
			// console.log("to reach on y :"+path[tick].y)
			player.position.y += 0.1
			player.position.y = Math.round(player.position.y * 100) / 100
			console.log(player.position.y)
		}
		// player.position.y = path[tick].y

		if(player.position.x == path[tick].x && player.position.y == path[tick].y){
			
			console.log("After for loop:")
			console.log('position x:'+player.position.x)
			console.log('position y:'+player.position.y)
			console.log('pos path x'+ path[tick].x)
			console.log('pos path y'+ path[tick].y)
			console.log('\n \n')
			tick++	
		} 
	}
	renderer.render(stage)
 	requestAnimationFrame(animate)	
}